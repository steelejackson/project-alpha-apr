from django.shortcuts import render, get_object_or_404, redirect

from projects.models import Project

from projects.forms import ProjectForm

from django.contrib.auth.decorators import login_required


# Create your views here.


@login_required
def list_projects(request):
    projects_list = Project.objects.filter(owner=request.user)
    context = {"projects_list": projects_list}
    return render(request, "lists/projects_list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project": project,
    }
    return render(request, "lists/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("home")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }

    return render(request, "lists/create.html", context)
